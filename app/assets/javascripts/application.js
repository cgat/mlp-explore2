// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require_tree .
//= require meat_base/application
//= require bootstrap


$(document).on('pjax:beforeSend', function() {
  $('.loading_overlay').css('visibility','visible');
  //in case the pjax call doesn't return, make area visible again
  setTimeout(function(){
    $('.loading_overlay').css('visibility','hidden');
  }, 10000);
});
$(document).on('pjax:complete', function() {
  $('.loading_overlay').css('visibility','hidden');
  $('#license_modal').appendTo('body');
});

//change the size
$(document).ready(function(){
  $('#toggle_nav_close, #toggle_nav_open').on('click', function() {
    setTimeout(function() {
      $('.loading_overlay').css("left",$('#nav-pane').width()+parseInt($('#nav-pane').css("borderRightWidth")));
    }, 400);
  });
});

//We appendTo body to get around an issue where modal aren't shown in the foreground
//if an ancestor has position: fixed.
//See http://stackoverflow.com/questions/10636667/bootstrap-modal-appearing-under-background
$(document).ready(function() {
  $('#license_modal').appendTo('body');
});


