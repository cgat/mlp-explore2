ComparisonHelper.class_eval do
  def partition_aligned(base_comparison, comparisons)
    base_capture_image = base_comparison.preview_capture_image
    raise ArgumentError, "Base comparison capture must contain at least one capture image" unless base_capture_image.present?
    comparisons_aligned, comparisons_unaligned = comparisons.partition do |cap|
      capture_image = cap.preview_capture_image
      capture_image.aligned?(base_capture_image) if capture_image.present?
    end
    return [comparisons_aligned, comparisons_unaligned]
  end
end
