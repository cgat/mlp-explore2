[CaptureImage, Capture, HistoricCapture, HistoricVisit, Location, Project, Station, Survey, SurveySeason, Surveyor, Visit].each do |klass|
  klass.class_eval do
    default_scope { published }
  end
end


