Station.class_eval do

  def latitude_with_caching
    Rails.cache.fetch([:station, id, :lat], expires_in: 1.day) do
      latitude_without_caching
    end
  end
  alias_method_chain :latitude, :caching

  def longitude_with_caching
    Rails.cache.fetch([:station, id, :long], expires_in: 1.day) do
      longitude_without_caching
    end
  end
  alias_method_chain :longitude, :caching

end
