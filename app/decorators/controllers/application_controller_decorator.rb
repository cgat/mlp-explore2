ApplicationController.class_eval do
  caches_action :show, expires_in: 1.day, layout: false

  def search_setup
    object_types_not_to_show = ['LocationImage','MetdataFile','FieldnoteFile']
    @search = session[:query].present? ? PgSearch.multisearch(session[:query]).to_a : []
    @search.reject!{|s| s.searchable.blank? || object_types_not_to_show.include?(s.searchable_type)}
  end


  def gmaps_markers_with_caching
    Rails.cache.fetch(:gmaps_markers, expires_in: 1.day) do
      gmaps_markers_without_caching
    end
  end
  alias_method_chain :gmaps_markers, :caching

end
