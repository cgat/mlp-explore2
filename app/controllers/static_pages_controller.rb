class StaticPagesController < ApplicationController
  caches_page :home

  def home
    render layout: "no_nav"
  end

end
