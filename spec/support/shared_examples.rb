shared_examples_for "default scope" do |klass|
  it "ensures the published scope is set as the default scope" do
    klass.scoped.to_sql == klass.published.to_sql
  end
end
