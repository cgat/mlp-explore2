set :application, 'mlp-explore2'
set :deploy_user, 'MountainLegacyProject'

#

# setup repo details
set :scm, :git
set :repo_url, 'git@bitbucket.org:cgat/mlp-explore2.git'

# setup memcached
set :memcached_memory_limit, 512
set :launch_agent_path, "~/Library/LaunchAgents/"

# setup rvm.

# how many old releases do we want to keep, not much
set :keep_releases, 5

# files we want symlinking to specific entries in shared
set :linked_files, %w{.env}

# dirs we want symlinking to shared
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# which config files should be copied by deploy:setup_config
# see documentation in lib/capistrano/tasks/setup_config.cap
# for details of operations
set(:config_files, %w(
  nginx.conf
  homebrew.mxcl.memcached.plist
))

# which config files should be made executable after copying
# by deploy:setup_config
set(:executable_config_files, [])


# files which need to be symlinked to other parts of the
# filesystem. For example nginx virtualhosts, log rotation
# init scripts etc. The full_app_name variable isn't
# available at this point so we use a custom template {{}}
# tag and then add it at run time.
set(:symlinks, [
  {
    source: "nginx.conf",
    link: "/usr/local/etc/nginx/"
  },
  {
    source: "homebrew.mxcl.memcached.plist",
    link: fetch(:launch_agent_path)
  }
])

# this:
# http://www.capistranorb.com/documentation/getting-started/flow/
# is worth reading for a quick overview of what tasks are called
# and when for `cap stage deploy`

namespace :deploy do
  # make sure we're deploying what we think we're deploying
  before :deploy, "deploy:check_revision"
  # only allow a deploy with passing tests to deployed
  before 'deploy:updated', 'bundler:install'
  #after 'deploy:symlink:shared', 'deploy:compile_assets'
  after 'deploy:publishing', 'deploy:restart'
  after :finishing, 'deploy:cleanup'
end


# ###
# set :application, 'my_app_name'
# set :repo_url, 'git@example.com:me/my_repo.git'

# # ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

# # set :deploy_to, '/var/www/my_app'
# # set :scm, :git

# # set :format, :pretty
# # set :log_level, :debug
# # set :pty, true

# # set :linked_files, %w{config/database.yml}
# # set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# # set :default_env, { path: "/opt/ruby/bin:$PATH" }
# # set :keep_releases, 5

# namespace :deploy do

#   desc 'Restart application'
#   task :restart do
#     on roles(:app), in: :sequence, wait: 5 do
#       # Your restart mechanism here, for example:
#       # execute :touch, release_path.join('tmp/restart.txt')
#     end
#   end

#   after :restart, :clear_cache do
#     on roles(:web), in: :groups, limit: 3, wait: 10 do
#       # Here we can do anything such as:
#       # within release_path do
#       #   execute :rake, 'cache:clear'
#       # end
#     end
#   end

#   after :finishing, 'deploy:cleanup'

# end
